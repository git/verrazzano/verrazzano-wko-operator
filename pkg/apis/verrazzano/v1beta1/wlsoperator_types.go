// Copyright (c) 2020, Oracle and/or its affiliates.
// Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

package v1beta1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// WlsOperatorSpec defines the desired state of WlsOperator
// +k8s:openapi-gen=true
type WlsOperatorSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book.kubebuilder.io/beyond_basics/generating_crd.html

	// User defined description of the the WlsOperator custom resource
	Description string `json:"description"`
	// The name of the WebLogic operator
	Name string `json:"name"`
	// The namespace for the Weblogic operator
	Namespace string `json:"namespace"`
	// The service account for the Weblogic operator
	ServiceAccount string `json:"serviceAccount"`
	// The docker image to use for the Weblogic operator
	Image string `json:"image,omitempty"`
	// The Kubernetes image pull policy. Defaults to IfNotPresent
	ImagePullPolicy corev1.PullPolicy `json:"imagePullPolicy,omitempty"`
	// The Kubernetes docker secret for pulling the Weblogic operator
	ImagePullSecret string `json:"imagePullSecret,omitempty"`
	// String array of domain namespaces that the Weblogic operator watches
	// +listType=set
	DomainNamespaces []string `json:"domainNamespaces,omitempty"`
}

// WlsOperatorStatus defines the observed state of WlsOperator
// +k8s:openapi-gen=true
type WlsOperatorStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "operator-sdk generate k8s" to regenerate code after modifying this file
	// Add custom validation using kubebuilder tags: https://book.kubebuilder.io/beyond_basics/generating_crd.html

	// State of the WLS operator deployment
	State string `json:"state,omitempty"`
	// Message associated with latest action
	LastActionMessage string `json:"lastActionMessage,omitempty"`
	// Time stamp for latest action
	LastActionTime string `json:"lastActionTime,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// WlsOperator is the Schema for the wlsoperators API
// +k8s:openapi-gen=true
// +kubebuilder:subresource:status
// +kubebuilder:resource:shortName=wo
// +genclient
// +genclient:noStatus
type WlsOperator struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   WlsOperatorSpec   `json:"spec,omitempty"`
	Status WlsOperatorStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// WlsOperatorList contains a list of WlsOperator
type WlsOperatorList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []WlsOperator `json:"items"`
}

func init() {
	SchemeBuilder.Register(&WlsOperator{}, &WlsOperatorList{})
}
