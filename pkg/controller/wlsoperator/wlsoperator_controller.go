// Copyright (c) 2020, Oracle and/or its affiliates.
// Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

package wlsoperator

import (
	"context"
	"fmt"
	"os/exec"
	"strings"
	"time"

	verrazzanov1beta1 "github.com/verrazzano/verrazzano-wko-operator/pkg/apis/verrazzano/v1beta1"
	"go.uber.org/zap"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/manager"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	"sigs.k8s.io/controller-runtime/pkg/source"
)

// Add creates a new WlsOperator Controller and adds it to the Manager. The Manager will set fields on the Controller
// and Start it when the Manager is Started.
func Add(mgr manager.Manager) error {
	return add(mgr, newReconciler(mgr))
}

// newReconciler returns a new reconcile.Reconciler
func newReconciler(mgr manager.Manager) reconcile.Reconciler {
	return &ReconcileWlsOperator{client: mgr.GetClient(), scheme: mgr.GetScheme()}
}

// add adds a new Controller to mgr with r as the reconcile.Reconciler
func add(mgr manager.Manager, r reconcile.Reconciler) error {
	// Create a new controller
	c, err := controller.New("wlsoperator-controller", mgr, controller.Options{Reconciler: r})
	if err != nil {
		return err
	}

	// Watch for changes to primary resource WlsOperator
	err = c.Watch(&source.Kind{Type: &verrazzanov1beta1.WlsOperator{}}, &handler.EnqueueRequestForObject{})
	if err != nil {
		return err
	}

	return nil
}

// blank assignment to verify that ReconcileWlsOperator implements reconcile.Reconciler
var _ reconcile.Reconciler = &ReconcileWlsOperator{}

// ReconcileWlsOperator reconciles a WlsOperator object
type ReconcileWlsOperator struct {
	// This client, initialized using mgr.Client() above, is a split client
	// that reads objects from the cache and writes to the apiserver
	client client.Client
	scheme *runtime.Scheme
}

const finalizer = "wlsoperators.verrazzano.io"

// Reconcile reads that state of the cluster for a WlsOperator object and makes changes based on the state read
// and what is in the WlsOperator.Spec
// Note:
// The Controller will requeue the Request to be processed again if the returned error is non-nil or
// Result.Requeue is true, otherwise upon completion it will remove the work from the queue.
func (r *ReconcileWlsOperator) Reconcile(request reconcile.Request) (reconcile.Result, error) {
	reqLogger := zap.S().With("Request.Namespace", request.Namespace, "Request.Name", request.Name)
	reqLogger.Infow("Reconciling WlsCluster")

	// Fetch the WlsOperator CR
	instance := &verrazzanov1beta1.WlsOperator{}
	err := r.client.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		// If the resource is not found, that means all of
		// the finalizers have been removed, and the WlsOperator
		// resource has been deleted, so there is nothing left
		// to do.
		if errors.IsNotFound(err) {
			return reconcile.Result{}, nil
		}

		// Error reading the object - requeue the request.
		reqLogger.Errorf("Failed to get WlsOperator, Error: %s", err.Error())
		return reconcile.Result{}, err
	}

	// Check if the WlsOperator CR is marked to be deleted, which is
	// indicated by the deletion timestamp being set.
	isMarkedToBeDeleted := instance.GetDeletionTimestamp() != nil
	if isMarkedToBeDeleted {
		if contains(instance.GetFinalizers(), finalizer) {
			// Run finalization logic for the finalizer. If the
			// finalization logic fails, don't remove the finalizer so
			// that we can retry during the next reconciliation.
			if err := r.runFinalizer(reqLogger, instance); err != nil {
				return reconcile.Result{}, err
			}

			// Remove finalizer. Once all finalizers have been
			// removed, the object will be deleted.
			instance.SetFinalizers(remove(instance.GetFinalizers(), finalizer))
			err := r.client.Update(context.TODO(), instance)
			if err != nil {
				return reconcile.Result{}, err
			}
		}
		return reconcile.Result{}, nil
	}

	// Check if the namespace for the WLS operator exists, if not found create it
	// Define a new Namespace object
	namespaceFound := &corev1.Namespace{}
	reqLogger.Infow("Checking if namespace exist")
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: instance.Spec.Namespace}, namespaceFound)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Infof("Creating a new namespace, Namespace: %s", instance.Spec.Namespace)
		err = r.client.Create(context.TODO(), newNamespace(instance))
		if err != nil {
			return reconcile.Result{}, err
		}

		// Namespace created successfully - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

	// Check if the serviceaccount for the WLS operator exists, if not found create it
	// Define a new ServiceAccount object
	saFound := &corev1.ServiceAccount{}
	reqLogger.Infow("Checking if serviceaccount exist")
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: instance.Spec.ServiceAccount, Namespace: instance.Spec.Namespace}, saFound)
	if err != nil && errors.IsNotFound(err) {
		reqLogger.Infof("Creating a new serviceaccount, Name: %s Namespace: %s", instance.Spec.ServiceAccount, instance.Spec.Namespace)
		err = r.client.Create(context.TODO(), newServiceAccount(instance))
		if err != nil {
			return reconcile.Result{}, err
		}

		// serviceaccount created successfully - return and requeue
		return reconcile.Result{Requeue: true}, nil
	} else if err != nil {
		return reconcile.Result{}, err
	}

	// Check if the WLS operator deployment already exists, if not create a new one via Helm
	reqLogger.Infow("Checking if deployment exists")
	deployFound := &appsv1.Deployment{}
	err = r.client.Get(context.TODO(), types.NamespacedName{Name: "weblogic-operator", Namespace: instance.Spec.Namespace}, deployFound)
	if err != nil && errors.IsNotFound(err) {
		// Create the helm arg list for deploying the WLS operator
		args := getCreateArgs(instance)

		// Execute a Helm install command to deploy the WLS operator
		reqLogger.Infof("Deploying WLS operator with helm, Args: %s", args)
		cmd := exec.Command("helm", args...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			reqLogger.Errorf(string(out)+", Error: %s", err.Error())
			r.updateStatus(reqLogger, instance, "Failed", "WLS operator deployment failed: "+string(out))
			return reconcile.Result{}, err
		}

		r.updateStatus(reqLogger, instance, "Deployed", "WLS operator deployed successfully")

		// Add finalizer for this CR since at this point the WLS operator has been deployed
		if !contains(instance.GetFinalizers(), finalizer) {
			if err := r.addFinalizer(reqLogger, instance); err != nil {
				return reconcile.Result{}, err
			}
		}

		// Deployment created successfully - don't requeue
		return reconcile.Result{}, nil
	} else if err != nil {
		reqLogger.Errorf("Failed to get WLS Operator Deployment, Error: %s", err)
		return reconcile.Result{}, err
	}

	// Create the helm arg list for upgrading the WLS operator
	args, err := r.getUpgradeArgs(reqLogger, instance, deployFound)
	if err != nil {
		return reconcile.Result{}, err
	}
	if args != nil {
		// Execute the Helm update command for any updates needed
		reqLogger.Infof("Upgrading WLS operator with helm, Args: %s", args)
		cmd := exec.Command("helm", args...)
		out, err := cmd.CombinedOutput()
		if err != nil {
			reqLogger.Errorf(string(out)+", Error: %s", err.Error())
			r.updateStatus(reqLogger, instance, instance.Status.State, "WLS operator deployment update failed: "+string(out))
			return reconcile.Result{}, err
		}

		r.updateStatus(reqLogger, instance, "Updated", "WLS operator deployment updated")

		// Deployment updated successfully - don't requeue
		return reconcile.Result{}, nil
	}

	// Deployment already exists - don't requeue
	reqLogger.Infof("Skip reconcile: WLS operator already exists, Name: %s Namespace: %s", instance.Spec.Name, instance.Spec.Namespace)
	return reconcile.Result{}, nil
}

// createNamespace returns a namespace resource that may need to be created
func newNamespace(cr *verrazzanov1beta1.WlsOperator) *corev1.Namespace {
	labels := make(map[string]string)
	labels["istio-injection"] = "enabled"

	namespace := &corev1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name:   cr.Spec.Namespace,
			Labels: labels,
		},
	}

	return namespace
}

// createServiceAccount returns a serviceaccount resource that may need to be created
func newServiceAccount(cr *verrazzanov1beta1.WlsOperator) *corev1.ServiceAccount {
	return &corev1.ServiceAccount{
		ObjectMeta: metav1.ObjectMeta{
			Name:      cr.Spec.ServiceAccount,
			Namespace: cr.Spec.Namespace,
		},
	}
}

// Return list of helm args needed for creating the operator
func getCreateArgs(cr *verrazzanov1beta1.WlsOperator) []string {
	var args []string
	args = append(args, "install", cr.Spec.Name, "resources/charts/weblogic-operator",
		"--namespace", cr.Spec.Namespace,
		"--set", fmt.Sprintf("serviceAccount=%s", cr.Spec.ServiceAccount),
		"--set", "istioEnabled=true")

	// domainNamespaces is a comma delimited list of namespaces
	args = append(args, "--set", fmt.Sprintf("domainNamespaces={%s}", strings.Join(cr.Spec.DomainNamespaces, ",")))

	if len(cr.Spec.Image) != 0 {
		args = append(args, "--set", fmt.Sprintf("image=%s", cr.Spec.Image))
	}

	if len(cr.Spec.ImagePullSecret) != 0 {
		args = append(args, "--set", fmt.Sprintf("imagePullSecrets[0].name=%s", cr.Spec.ImagePullSecret))
	}

	if len(cr.Spec.ImagePullPolicy) != 0 {
		args = append(args, "--set", fmt.Sprintf("imagePullPolicy=%s", cr.Spec.ImagePullPolicy))
	}
	return args
}

// Return list of helm args needed for upgrading the operator
func (r *ReconcileWlsOperator) getUpgradeArgs(reqLogger *zap.SugaredLogger, cr *verrazzanov1beta1.WlsOperator, deployFound *appsv1.Deployment) ([]string, error) {
	var args []string
	args = append(args, "upgrade", cr.Spec.Name, "resources/charts/weblogic-operator", "--reuse-values", "--namespace", cr.Spec.Namespace)
	fixedArgs := len(args)

	// Check if image needs updating
	if deployFound.Spec.Template.Spec.Containers[0].Image != cr.Spec.Image {
		args = append(args, "--set", fmt.Sprintf("image=%s", cr.Spec.Image))
	}

	// Check if imagePullSecrets needs updating
	pullSecrets := ""
	if len(deployFound.Spec.Template.Spec.ImagePullSecrets) != 0 {
		pullSecrets = deployFound.Spec.Template.Spec.ImagePullSecrets[0].Name
	}
	if pullSecrets != cr.Spec.ImagePullSecret {
		args = append(args, "--set", fmt.Sprintf("imagePullSecrets[0].name=%s", cr.Spec.ImagePullSecret))
	}

	// Check if imagePullPolicy needs updating
	if len(cr.Spec.ImagePullPolicy) == 0 {
		cr.Spec.ImagePullPolicy = corev1.PullIfNotPresent
	}
	if deployFound.Spec.Template.Spec.Containers[0].ImagePullPolicy != cr.Spec.ImagePullPolicy {
		args = append(args, "--set", fmt.Sprintf("imagePullPolicy=%s", cr.Spec.ImagePullPolicy))
	}

	// Check if domainNamespaces needs updating
	// domainNamspaces are stored in a configmap
	cmFound := &corev1.ConfigMap{}
	reqLogger.Infow("Getting configmap for WLS operator")
	err := r.client.Get(context.TODO(), types.NamespacedName{Namespace: cr.Spec.Namespace, Name: "weblogic-operator-cm"}, cmFound)
	if err != nil {
		return nil, err
	}

	cmNamespacesPreSplit := ""
	for key, value := range cmFound.Data {
		// targetNamespaces holds a comma seperated list of namespace strings
		if key == "targetNamespaces" {
			cmNamespacesPreSplit = strings.TrimSpace(value)
			break
		}
	}
	var cmNamespaces []string
	if len(cmNamespacesPreSplit) != 0 {
		cmNamespaces = strings.Split(cmNamespacesPreSplit, ",")
	}

	if len(cmNamespaces) > 0 || len(cr.Spec.DomainNamespaces) > 0 {
		// Use what is in the CR if the configmap and CR are different sizes
		if len(cmNamespaces) != len(cr.Spec.DomainNamespaces) {
			args = append(args, "--set", fmt.Sprintf("domainNamespaces={%s}", strings.Join(cr.Spec.DomainNamespaces, ",")))
		} else {
			//Check for mismatch between the configmap and CR
			match := false
			for _, cmNamespace := range cmNamespaces {
				match = false
				for _, crNamespace := range cr.Spec.DomainNamespaces {
					if cmNamespace == crNamespace {
						match = true
						break
					}
				}
				if !match {
					break
				}
			}

			// If there is a mismatch between the configmap and CR then use what is in the CR
			if !match {
				args = append(args, "--set", fmt.Sprintf("domainNamespaces={%s}", strings.Join(cr.Spec.DomainNamespaces, ",")))
			}
		}
	}

	if len(args) > fixedArgs {
		return args, nil
	}

	return nil, nil
}

// Update the status for the CR
func (r *ReconcileWlsOperator) updateStatus(reqLogger *zap.SugaredLogger, cr *verrazzanov1beta1.WlsOperator, state string, message string) error {
	cr.Status.State = state
	cr.Status.LastActionMessage = message
	t := time.Now().UTC()
	cr.Status.LastActionTime = fmt.Sprintf("%d-%02d-%02dT%02d:%02d:%02dZ",
		t.Year(), t.Month(), t.Day(),
		t.Hour(), t.Minute(), t.Second())

	// Update status in CR
	err := r.client.Status().Update(context.TODO(), cr)
	if err != nil {
		reqLogger.Errorf("Failed to update WLS operator status, Error: %s", err.Error())
		return err
	}
	return nil

}

// Add finalizer to CR for cleanup when resource is deleted
func (r *ReconcileWlsOperator) addFinalizer(reqLogger *zap.SugaredLogger, cr *verrazzanov1beta1.WlsOperator) error {
	reqLogger.Infow("Adding finalizer " + finalizer)
	cr.SetFinalizers(append(cr.GetFinalizers(), finalizer))

	// Update CR
	err := r.client.Update(context.TODO(), cr)
	if err != nil {
		reqLogger.Errorf("Failed to add finalizer, Error: %s", err)
		return err
	}
	return nil
}

// Cleanup code (finalizer) when CR is deleted
func (r *ReconcileWlsOperator) runFinalizer(reqLogger *zap.SugaredLogger, cr *verrazzanov1beta1.WlsOperator) error {
	reqLogger.Infow("Running finalizer " + finalizer)
	args := []string{"delete", cr.Spec.Name, "--namespace", cr.Spec.Namespace}
	reqLogger.Infof("Deleting WLS operator with helm, Args: %s", args)
	cmd := exec.Command("helm", args...)
	out, err := cmd.CombinedOutput()
	if err != nil {
		reqLogger.Errorf(string(out)+", Error: %s", err.Error())
		r.updateStatus(reqLogger, cr, "Failed", "WLS operator deployment deletion failed: "+string(out))
		return err
	}

	return nil
}

// Check if string is found in list
func contains(list []string, s string) bool {
	for _, v := range list {
		if v == s {
			return true
		}
	}
	return false
}

// Remove string from list
func remove(list []string, s string) []string {
	for i, v := range list {
		if v == s {
			list = append(list[:i], list[i+1:]...)
		}
	}
	return list
}
