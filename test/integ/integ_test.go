// Copyright (c) 2020, Oracle and/or its affiliates.
// Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

package integ_test

import (
	"bytes"
	"context"
	"errors"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	apixv1beta1client "k8s.io/apiextensions-apiserver/pkg/client/clientset/clientset/typed/apiextensions/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

var _ = Describe("Verrazzano WKO operator", func() {
	It("is deployed", func() {
		deployment, err := getClientSet().AppsV1().Deployments("verrazzano-system").Get(context.Background(), "verrazzano-wko-operator", metav1.GetOptions{})
		Expect(err).To(BeNil(), "Should not have received an error when trying to get the wls-operator deployment")
		Expect(deployment.Spec.Template.Spec.Containers[0].Command[0]).To(Equal("verrazzano-wko-operator"),
			"Should have a container with command=verrazzano-wko-operator")
	})

	It("is running (within 1m)", func() {
		isPodRunningYet := func() bool {
			return isPodRunning("verrazzano-wko-operator", "verrazzano-system")
		}
		Eventually(isPodRunningYet, "1m", "5s").Should(BeTrue(),
			"The verrazzano-wko-operator pod should be in the Running state")
	})
})

var _ = Describe("Custom Resource Definition", func() {
	It("wlsoperators.verrazzano.io exists", func() {
		Expect(doesCRDExist("wlsoperators.verrazzano.io")).To(BeTrue(),
			"The wlsoperators.verrazzano.io CRD should exist")
	})
})

var _ = Describe("  A haiku should have", func() {
	Context("\n  a story about nature,", func() {
		It("juxtaposition", func() {
			Expect(1).To(Equal(1),
				"Poetry matters in these testing times")
		})
	})
})

var _ = Describe("A WlsOperator custom resource", func() {
	It("can be created", func() {
		createNamespace("verrazzano-bobs-books-binding")
		//err := createWlsOperatorCustomResource()
		//Expect(err).To(BeNil(),
		//	fmt.Sprintf("Should not have got an error when creating WlsOperator custom resource, but received \n%s\n", err.Error()))
		// MARK: getting 404 on this ^^ wondering if apix is not there in kind, trying straight kubectl instead for a workaround
		createWlsOperatorCustomResourceUsingKubectl()
		stdout, _ := getWlsOperatorCustomResourcesUsingKubectl("verrazzano-bobs-books-binding")
		Expect(stdout).To(ContainSubstring("wls-operator"),
			"Should have found a WlsOperator CR called wls-operator in namespace verrazzano-bobs-books-binding")
	})

	It("results in the WKO micro-operator deploying the WebLogic Kubernetes Operator (within 2 minutes)", func() {
		name := "weblogic-operator"
		namespace := "verrazzano-bobs-books-binding"

		//Eventually(doesDeploymentExist("a", "default")).Should(BeTrue(),
		//	"testing...")
		isDeploymentThereYet := func() bool {
			return doesDeploymentExist(name, namespace)
		}
		Eventually(isDeploymentThereYet, "2m", "5s").Should(BeTrue(),
			"There should have been a deployment called "+name+" in namespace "+namespace)

		deployment, _ := getDeployment(name, namespace)
		Expect(deployment.Spec.Template.Spec.Containers[0].Image).To(Equal("oracle/weblogic-kubernetes-operator:3.0.2"),
			"Should have a container with image=oracle/weblogic-kubernetes-operator:3.0.2")

		isPodRunningYet := func() bool {
			return isPodRunning(name, namespace)
		}
		Eventually(isPodRunningYet, "2m", "5s").Should(BeTrue(),
			"There should have been a pod named "+name+"* in the running state")

		// check domain namespaces are correct ...

		// check the wko crd was created ...

	})
})

// ---------------------------   helper functions ------------------------------------

func getKubeconfig() string {
	var kubeconfig string
	if home := homeDir(); home != "" {
		kubeconfig = filepath.Join(home, ".kube", "config")
	} else {
		Fail("Could not get kubeconfig")
	}
	return kubeconfig
}

func getClientSet() *kubernetes.Clientset {
	kubeconfig := getKubeconfig()

	// use the current context in the kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		Fail("Could not get current context from kubeconfig " + kubeconfig)
	}

	// create the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		Fail("Could not get clientset from config")
	}

	return clientset
}

func homeDir() string {
	if h := os.Getenv("HOME"); h != "" {
		return h
	}
	return ""
}

func doesCRDExist(crdName string) bool {
	kubeconfig := getKubeconfig()

	// use the current context in the kubeconfig
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		Fail("Could not get current context from kubeconfig " + kubeconfig)
	}

	apixClient, err := apixv1beta1client.NewForConfig(config)
	if err != nil {
		Fail("Could not get apix client")
	}

	crds, err := apixClient.CustomResourceDefinitions().List(context.Background(), metav1.ListOptions{})
	if err != nil {
		Fail("Failed to get list of CustomResourceDefinitions")
	}

	for i := range crds.Items {
		if strings.Compare(crds.Items[i].ObjectMeta.Name, crdName) == 0 {
			return true
		}
	}

	return false
}

func doesDeploymentExist(name string, namespace string) bool {
	GinkgoWriter.Write([]byte("[DEBUG] looking for deployment " + name + " in " + namespace + "\n"))
	_, err := getDeployment(name, namespace)
	if err != nil {
		return false
	}
	return true
}

func getDeployment(name string, namespace string) (*appsv1.Deployment, error) {
	return getClientSet().AppsV1().Deployments(namespace).Get(context.Background(), name, metav1.GetOptions{})
}

func isPodRunning(name string, namespace string) bool {
	GinkgoWriter.Write([]byte("[DEBUG] checking if there is a running pod named " + name + "* in namespace " + namespace + "\n"))
	clientset := getClientSet()
	pods, err := clientset.CoreV1().Pods(namespace).List(context.Background(), metav1.ListOptions{})
	if err != nil {
		Fail("Could not get list of pods")
	}
	for i := range pods.Items {
		if strings.HasPrefix(pods.Items[i].Name, name) {
			conditions := pods.Items[i].Status.Conditions
			for j := range conditions {
				if conditions[j].Type == "Ready" {
					if conditions[j].Status == "True" {
						return true
					}
				}
			}
		}
	}
	return false
}

func createNamespace(name string) {
	_, err := getClientSet().CoreV1().Namespaces().Create(
		context.Background(),
		&corev1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: name,
			},
		}, metav1.CreateOptions{})
	if err != nil {
		Fail("Could not create namespace " + name)
	}
}

func createWlsOperatorCustomResourceUsingKubectl() {
	runCommand("kubectl apply -f data/wlsoperator.cr.yaml")
}

func getWlsOperatorCustomResourcesUsingKubectl(namespace string) (string, string) {
	return runCommand("kubectl get wlsoperators.verrazzano.io -n " + namespace)
}

var wlsOperatorGVR = schema.GroupVersionResource{
	Group:    "verrazzano.io",
	Version:  "v1beta1",
	Resource: "wlsoperators",
}

func createWlsOperatorCustomResource() error {
	config, err := clientcmd.BuildConfigFromFlags("", getKubeconfig())
	if err != nil {
		return errors.New("Could not get client from kubeconfig")
	}

	client, err := dynamic.NewForConfig(config)
	if err != nil {
		return errors.New("Could not get dynamic client")
	}

	res := client.Resource(wlsOperatorGVR)
	wo := newWlsOperatorCustomResource()

	_, err = res.Create(context.Background(), wo, metav1.CreateOptions{})
	if err != nil {
		return err
		//return errors.New("Could not create WlsOperator custom resource")
	}

	return nil
}

func newWlsOperatorCustomResource() *unstructured.Unstructured {
	return &unstructured.Unstructured{
		Object: map[string]interface{}{
			"kind":       "wlsoperator",
			"apiVersion": "verrazzano.io/v1beta1",
			"metadata": map[string]interface{}{
				"name":      "wls-operator",
				"namespace": "verrazzano-bobs-books-binding1",
				"labels": map[string]interface{}{
					"k8s-app":            "verrazzano.io",
					"verrazzano.binding": "bobs-books-binding1",
					"verrazzano.cluster": "early1-managed-1",
				},
			},
			"spec": map[string]interface{}{
				"description":      "WebLogic Operator for managed cluster early1-managed-1 using binding bobs-books-binding1",
				"domainNamespaces": []interface{}{"bob", "bobby"},
				"image":            "oracle/weblogic-kubernetes-operator:3.0.2",
				"imagePullPolicy":  "IfNotPresent",
				"name":             "wls-operator-bobs-books-binding1",
				"namespace":        "verrazzano-bobs-books-binding1",
				"serviceAccount":   "wls-operator",
			},
		},
	}
}

// RunCommand runs an external process, captures the stdout
// and stderr, as well as streaming them to the real output
// streams in real time
func runCommand(commandLine string) (string, string) {
	GinkgoWriter.Write([]byte("[DEBUG] RunCommand: " + commandLine + "\n"))
	parts := strings.Split(commandLine, " ")
	var cmd *exec.Cmd
	if len(parts) < 1 {
		Fail("No command provided")
	} else if len(parts) == 1 {
		cmd = exec.Command(parts[0], "")
	} else {
		cmd = exec.Command(parts[0], parts[1:]...)
	}
	var stdoutBuf, stderrBuf bytes.Buffer
	stdoutIn, _ := cmd.StdoutPipe()
	stderrIn, _ := cmd.StderrPipe()

	var errStdout, errStderr error
	stdout := io.MultiWriter(os.Stdout, &stdoutBuf)
	stderr := io.MultiWriter(os.Stderr, &stderrBuf)
	err := cmd.Start()
	if err != nil {
		Fail("cmd.Start() failed with " + err.Error())
	}

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		_, errStdout = io.Copy(stdout, stdoutIn)
		wg.Done()
	}()

	_, errStderr = io.Copy(stderr, stderrIn)
	wg.Wait()

	err = cmd.Wait()
	if err != nil {
		Fail("cmd.Run() failed with " + err.Error())
	}
	if errStdout != nil || errStderr != nil {
		Fail("failed to capture stdout or stderr")
	}
	outStr, errStr := string(stdoutBuf.Bytes()), string(stderrBuf.Bytes())
	return outStr, errStr
}
