// Copyright (c) 2020, Oracle and/or its affiliates.
// Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

package temp_test

import (
	"fmt"
	"sync"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

var _ = Describe("wait for three things in parallel", func() {
	It("should also work", func() {
		Concurrently(
			func() { Eventually(divisibleByTwo, "15s", "1s").Should(BeTrue()) },
			func() { Eventually(divisibleByThree, "15s", "1s").Should(BeTrue()) },
			func() { Eventually(divisibleByFive, "15s", "1s").Should(BeTrue()) })
	})
})

func Concurrently(assertions ...func()) {
	number := len(assertions)
	var wg sync.WaitGroup
	wg.Add(number)
	for _, assertion := range assertions {
		go assert(&wg, assertion)
	}
	wg.Wait()
}

func assert(wg *sync.WaitGroup, assertion func()) {
	defer wg.Done()
	defer GinkgoRecover()
	assertion()
}

func divisibleByTwo() bool {
	return divisibleByN(2)
}

func divisibleByThree() bool {
	return divisibleByN(3)
}

func divisibleByFive() bool {
	return divisibleByN(65)
}

func divisibleByN(n int) bool {
	second := time.Now().Second()
	remainder := second % n
	GinkgoWriter.Write([]byte(fmt.Sprintf("second = %d, n = %d, remainder = %d\n", second, n, remainder)))
	if remainder == 0 {
		return true
	}
	return false
}
