// Copyright (c) 2020, Oracle and/or its affiliates.
// Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

package main

import (
	"io/ioutil"
	"os"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	kzap "sigs.k8s.io/controller-runtime/pkg/log/zap"
)

var _ = Describe("The main command", func() {
	It("printVersion works as expected", func() {
		// initialize to collect stderr
		oldStderr := os.Stderr
		r, w, _ := os.Pipe()
		os.Stderr = w

		InitLogs(kzap.Options{})
		printVersion()

		// collect stderr and reset
		w.Close()
		outputByte, _ := ioutil.ReadAll(r)
		output := string(outputByte)
		os.Stderr = oldStderr

		Expect(output).To(ContainSubstring("Operator Version"), "The log should contain the message `Operator Version`")
		Expect(output).To(ContainSubstring("Go Version: go1.13"), "The log should contain the message `Go Version: go1.13`")
		Expect(output).To(ContainSubstring("Version of operator-sdk: v0.18.1"), "The log should contain the message `Version of operator-sdk: v0.18.1`")
	})
})
