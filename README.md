[![Go Report Card](https://goreportcard.com/badge/github.com/verrazzano/verrazzano-wko-operator)](https://goreportcard.com/report/github.com/verrazzano/verrazzano-wko-operator)

# verrazzano-wko-operator

Kubernetes operator for handling WebLogic operators

## Prerequisites

operator-sdk version must be v0.18.1


## How to Build
```
make build
```

## How to run outside a Kubernetes cluster

```bash
export GO111MODULE=on

export OPERATOR_NAME=verrazzano-wko-operator
operator-sdk up local --namespace=""
```

## How to build and deploy in a Kubernetes cluster

```bash
operator-sdk build verrazzano-wko-operator:v0.0.1

sed -i -e 's|REPLACE_IMAGE|verrazzano-wko-operator:v0.0.1|g' deploy/operator.yaml
# This step is using the default namespace for the deployment. Change to desired
# namespace if need be.
sed -i -e "s|REPLACE_NAMESPACE|default|g" deploy/role_binding.yaml

kubectl apply -f deploy/service_account.yaml
kubectl apply -f deploy/role.yaml
kubectl apply -f deploy/role_binding.yaml
kubectl apply -f deploy/operator.yaml

```

## How to update the CRD

```bash
# Make edits to pkg/apis/verrazzano/v1beta1/wlsoperator_types.go

# Regenerate
make generate
```
